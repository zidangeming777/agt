package com.muhammadzidan.agt;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class pendataan_barang extends AppCompatActivity {
    ListView list_pendataan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pendataan_barang);

        list_pendataan = findViewById(R.id.list_pendataan);

        String[] values = new String[]{
                "Ayam", "Nasi", "Sambal Terasi", "Sambal Bawang", "Tempe", "Tahu",
                "Kulit", "Jamur", "Ikan Peda", "Kangkung"
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1
                ,android.R.id.text1, values);

        list_pendataan.setAdapter(adapter);
    }
}