package com.muhammadzidan.agt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;

public class Activity2 extends AppCompatActivity {
    ListView inventori;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        inventori = findViewById(R.id.inventori);

        String[] values = new String[]{
                "Ayam", "Nasi", "Sambal Terasi", "Sambal Bawang", "Tempe", "Tahu",
                "Kulit", "Jamur", "Ikan Peda", "Kangkung"
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1
                ,android.R.id.text1, values);

        inventori.setAdapter(adapter);

    }
}