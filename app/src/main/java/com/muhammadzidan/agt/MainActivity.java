package com.muhammadzidan.agt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {
    private ImageButton pendataan;
    private ImageButton inventori;
    private ImageButton inventori_k;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pendataan = (ImageButton) findViewById(R.id.pendataan);
        inventori = (ImageButton) findViewById(R.id.inventori);
        inventori_k = (ImageButton) findViewById(R.id.inventori_k);

        pendataan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity2();
            }
        });

        inventori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity3();
            }
        });

        inventori_k.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity4();
            }
        });
    }

    public void openActivity2() {
        Intent intent = new Intent(this, pendataan_barang.class);
        startActivity(intent);
    }

    public void openActivity3() {
        Intent intent = new Intent(this, Activity2.class);
        startActivity(intent);
    }

    public void openActivity4() {
        Intent intent = new Intent(this, inventori_keluar.class);
        startActivity(intent);
    }
}