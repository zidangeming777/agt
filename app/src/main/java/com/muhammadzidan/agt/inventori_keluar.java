package com.muhammadzidan.agt;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class inventori_keluar extends AppCompatActivity {

    ListView inventori_keluar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventori_keluar);

        inventori_keluar = findViewById(R.id.inventori_keluar);

        String[] values = new String[]{
                "Ayam Goreng Telur + Nasi + Sambal", "Ayam Bakar + Nasi + Sambal", "Kulit Krispi + Nasi + Sambal",
                "Ikan Peda + Nasi + Sambal", "Ayam Goreng Telur", "Kulit Krispi", "Jamur Krispi",
                "Tahu", "Tempe", "Ikan Peda", "Sambal",
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1
                ,android.R.id.text1, values);

        inventori_keluar.setAdapter(adapter);
    }
}